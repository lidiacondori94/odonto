<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda_model extends CI_Model {

    public function retornarAgenda()
    {
        $this->db->select('*');
        $this->db->from('agendacitas');
        // $this->db->where('estado',1);
        return $this->db->get();
    }
    public function agregarAgenda($data)
    {
        $this->db->insert('agendacitas',$data);
    }
    
    public function recuperarAgenda($idAgenda)
    {
        $this->db->select('*');
        $this->db->from('agendacitas');
        $this->db->where('idAgenda',$idAgenda);
        return $this->db->get();
    }
    
    public function modificarAgenda($idAgenda,$data)
    {
        $this->db->where('idAgenda',$idAgenda);
        $this->db->update('agendacitas',$data);
    }
    
    public function eliminarAgenda($idAgenda)
    {
        $this->db->where('idAgenda',$idAgenda);
        $this->db->delete('agendacitas');
    }

}