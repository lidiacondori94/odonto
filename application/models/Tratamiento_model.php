<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tratamiento_model extends CI_Model {

    public function retornarTratamiento()
    {
        $this->db->select('*');
        $this->db->from('tratamiento');
        // $this->db->where('estado',1);
        return $this->db->get();
    }
    public function agregarTratamiento($data)
    {
        $this->db->insert('tratamiento',$data);
    }

    public function getTratamiento($idTratamiento){
        $this->db->where("idTratamiento",$idTratamiento);
        $resultado = $this->db->get("tratamiento");
        return $resultado->row();
    }
    
    public function recuperarTratamiento($idTratamiento)
    {
        $this->db->select('*');
        $this->db->from('tratamiento');
        $this->db->where('idTratamiento',$idTratamiento);
        return $this->db->get();
    }
    
    public function modificarTratamiento($idTratamiento,$data)
    {
        $this->db->where('idTratamiento',$idTratamiento);
        $this->db->update('tratamiento',$data);
    }
    
    public function eliminarTratamiento($idTratamiento)
    {
        $this->db->where('idTratamiento',$idTratamiento);
        $this->db->delete('tratamiento');
    }

}