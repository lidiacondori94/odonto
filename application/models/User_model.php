<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function retornarUsuario()
    {
        $this->db->select("p.*,u.*");
        $this->db->from("Persona p");
        $this->db->join("Usuario u","p.idPersona = u.idUsuario");
        $this->db->where('estado',1);
        return $this->db->get();
    }
    // public function agregarUsuario($data)
    // {
    //     $this->db->trans_start();
    //     $this->db->insert('usuario',$data);

        
    //     $idUsuario=$this->db->insert_id();

    //     $this->db->where('idUsuario',$idUsuario);
    
    //     if ($this->db->trans_status() === FALSE)
    //     {
    //             $this->db->trans_rollback();
    //     }
    //     else
    //     {
    //             $this->db->trans_commit();
    //     }
    //     return $this->db->trans_status();
    // }

    ///para agregar a la persona y usuario
    public function agregarPersona($data)
    {
        return $this->db->insert('persona',$data);

    }
     public function ultimoIDPersona()
    {
         return $this->db->insert_id();
    }

    
     public function guardarUsuarioo($data)
    {
        $this->db->insert('usuario',$data);
    }
    
    public function recuperarUsuario($idUsuario)
    {

        $this->db->select("p.*,u.*");
        $this->db->from("Persona p");
        $this->db->join("Usuario u","p.idPersona = u.idUsuario");
        $this->db->where('estado',1);
        $this->db->where('idUsuario',$idUsuario);            
        return $this->db->get();
    }
    
    public function modificarUsuario($idUsuario,$data)
    {
        $this->db->where('idUsuario',$idUsuario);
        $this->db->update('usuario',$data);
    }
    
    public function eliminarUsuario($idUsuario,$data)
    {
        $this->db->where('idUsuario',$idUsuario);
        $this->db->update('usuario',$data);
    }    

    // public function getRol()//para extraer datos de otra tabla en un combo box
    // {
    //     $resultados=$this->db-> get('usuario');
    //     return $resultados->result();
    // }
} 


