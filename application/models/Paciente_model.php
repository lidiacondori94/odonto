<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paciente_model extends CI_Model {

    public function retornarPaciente()
    {      
        $this->db->select("p.*,pa.*");
        $this->db->from("persona p");
        $this->db->join("paciente pa","p.idPersona = pa.idPaciente");
        $this->db->where('estado',1);
        return $this->db->get();
    
    }
    public function agregarPersona($data)
    {
        return $this->db->insert('persona',$data);

    }

     public function ultimoIDPersona()
    {
         return $this->db->insert_id();
    }

     public function guardarPacientee($data)
    {
        $this->db->insert('paciente',$data);
    }

    // public function agregarPaciente($data)
    // {
    //     $this->db->insert('paciente',$data);
    // }

    
    public function recuperarPaciente($idPaciente)
    {
        $this->db->select('*');
        $this->db->from('paciente');
        $this->db->where('idPaciente',$idPaciente);
        return $this->db->get();
    }
    
    public function modificarPaciente($idPaciente,$data)
    {
        $this->db->where('idPaciente',$idPaciente);
        $this->db->update('paciente',$data);
    }
    
    public function eliminarPaciente($idPaciente)
    {
        $this->db->where('idPaciente',$idPaciente);
        $this->db->delete('paciente');
    }

}