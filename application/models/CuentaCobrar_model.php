<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CuentaCobrar_model extends CI_Model {

    public function retornarCuentaCobrar()
    {
        $this->db->select('*');
        $this->db->from('cuentaporcobrar');
        $this->db->where('estado',1);
        return $this->db->get();
    }
    public function agregarCuentaCobrar($data)
    {
        $this->db->insert('cuentaporcobrar',$data);
    }
    
    public function recuperarCuentaCobrar($idCuentaPorCobrar)
    {
        $this->db->select('*');
        $this->db->from('cuentaporcobrar');
        $this->db->where('idCuentaPorCobrar',$idCuentaPorCobrar);
        return $this->db->get();
    }
    
    public function modificarCuentaCobrar($idCuentaPorCobrar,$data)
    {
        $this->db->where('idCuentaPorCobrar',$idCuentaPorCobrar);
        $this->db->update('cuentaporcobrar',$data);
    }
    
    public function eliminarCuentaCobrar($idCuentaPorCobrar)
    {
        $this->db->where('idCuentaPorCobrar',$idCuentaPorCobrar);
        $this->db->delete('cuentaporcobrar');
    }

}