<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

    // obtener Persona
    public function getUsuario(){
        $this->db->where("estado","1");
        $resultados = $this->db->get("usuario");
        return $resultados->result();
    }
    // salvar
    public function save($data){
        return $this->db->insert("usuario",$data);

    }

    // obtener Persona
    public function getUsuarios($idUsuario){
        $this->db->where("idUsuario",$idUsuario);
        $resultados =$this->db->get("usuario");
        return $resultados->row();   
    }

    // actualizar
    public function update($idUsuario,$data){
        $this->db->where("idUsuario",$idUsuario);
        return $this->db->update("usuario",$data);

    }

    public function login($nombreUsuario, $password){
        $this->db->where("nombreUsuario", $nombreUsuario);
        $this->db->where("password", $password);

        $resultados = $this->db->get("usuario");
        if( $resultados->num_rows() > 0){
            return $resultados->row();
        }
        else{
            return false;
        }
    }
}