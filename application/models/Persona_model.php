<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persona_model extends CI_Model {

    public function retornarPersona()
    {
        $this->db->select('*');
        $this->db->from('persona');
        // $this->db->where('estado',1);
        return $this->db->get();
    }
    public function agregarPersona($data)
    {
        $this->db->trans_start();
        $this->db->insert('persona',$data);

        
        $idPersona=$this->db->insert_id();

        $this->db->where('idPersona',$idPersona);
    
        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
        }
        else
        {
                $this->db->trans_commit();
        }
        return $this->db->trans_status();
    }
    
    public function recuperarPersona($idPersona)
    {
        $this->db->select('*');
        $this->db->from('persona');
        $this->db->where('idPersona',$idPersona);
        return $this->db->get();
    }
    
    public function modificarPersona($idPersona,$data)
    {
        $this->db->where('idPersona',$idPersona);
        $this->db->update('persona',$data);
    }
    
    public function eliminarPersona($idPersona,$data)
    {
        $this->db->where('idPersona',$idPersona);
        $this->db->update('persona',$data);
    }



//para detalle venta
 //    public function getCliente($idCliente)
    // {
    //  $this->db->where('idCliente',$idCliente);
    //  $resul=$this->db->get('cliente');
    //  return $resul->row();
 //    }
    
}
