


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial_model extends CI_Model {

     public function retornarPaciente()
    {      
        $this->db->select("p.*,pa.*");
        $this->db->from("persona p");
        $this->db->join("paciente pa","p.idPersona = pa.idPaciente");
        $this->db->where('estado',1);
        return $this->db->get();
    }


	public function getTratamiento()//para extraeer los datos de la otra tabla join
	{
		$resultados=$this->db->get('tratamiento');
		return $resultados->result();
	}
    public function getHistorial($valor){
        $this->db->select("idTratamiento,nombre as label,descripcion,precio");
        $this->db->from("tratamiento");
        $this->db->like("nombre",$valor);
        $resultados = $this->db->get();
        return $resultados->result_array();
        
    
    }

    public function retornarHistoriaClinica()
    {
        $this->db->select('*');
        $this->db->from('historiaclinica');
        // $this->db->where('estado',1);
        return $this->db->get();
    }


    public function agregarHistoriall($data)
    {
        return $this->db->insert('historiaclinica',$data);
    }
     //ultimo id de la tabla ventas
    public function ultimoID()
    {
        return $this->db->insert_id();
    }

    public function guardarHistorialPaciente($data)
    {
        $this->db->insert('historialpaciente',$data);
    }







}