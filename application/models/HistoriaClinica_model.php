<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HistoriaClinica_model extends CI_Model {

    public function retornarHistoria()
    {
        $this->db->select('hc.*, pa.*,p.*');
        $this->db->from('historiaClinica hc');       
        $this->db->join("paciente pa","hc.idPaciente = pa.idPaciente");   
        $this->db->join("persona p","p.idPersona = pa.idPaciente");      
        return $this->db->get();
    }

    public function getPaciente()//para extraer datos de otra tabla en un combo box
    {
        $resultados=$this->db-> get('paciente');
        return $resultados->result();
    }

    public function agregarHistoria($data)
    {
        $this->db->insert('historiaClinica',$data);
    }
    
    public function recuperarHistoria($idHistoriaClinica)
    {
        $this->db->select('*');
        $this->db->from('historiaClinica');
        $this->db->where('idHistoriaClinica',$idHistoriaClinica);
        return $this->db->get();
    }
    
    public function modificarHistoria($idHistoriaClinica,$data)
    {
        $this->db->where('idHistoriaClinica',$idHistoriaClinica);
        $this->db->update('historiaClinica',$data);
    }
    
    public function eliminarHistoria($idHistoriaClinica)
    {
        $this->db->where('idHistoriaClinica',$idHistoriaClinica);
        $this->db->delete('historiaClinica');
    }

}