<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

public function __construct(){
    parent::__construct();
    $this->load->model("Usuarios_model");
}
	public function index()
	{
		if ($this->session->userdata("login")){
            redirect(base_url()."dashboard");
        }
        else{
	        $this->load->view('admin/login');
        }	
		

	}
    public function login(){
        $nombreUsuario = $this->input->post("nombreUsuario");
        $password = $this->input->post("password");     
        $res = $this->Usuarios_model->login($nombreUsuario, md5($password));

        if (!$res){
            $this->session->set_flashdata("error","Usuario y/o Contraseña----- incorrectos");
            redirect(base_url());
        } 
        else{
            
            $data = array(
                'id' => $res->idUsuario,
                'nombreUsuario' => $res->nombreUsuario,                
                'rol' => $res->rol,
                'login' => TRUE
            );
            $this->session->set_userdata($data);
            redirect(base_url()."dashboard");
        }

    }
        
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }
}