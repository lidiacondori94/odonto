<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Piezas extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
         $this->load->model("Pieza_model");
  
}

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('container');
        $this->load->view('layouts/footer');
    }
    public function listaPieza()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');   
        $data['piezadental']=$this->Pieza_model->retornarPieza();    
        $this->load->view('admin/piezaDental/list',$data);
        $this->load->view('layouts/footer');
    }
    
     
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/piezaDental/add');
        $this->load->view('layouts/footer');
    }

    public function agregardb()
    {
        $nombrePieza=$_POST['nombrePieza'];
        $data['nombrePieza']=$nombrePieza;
   
        
        
        $this->Pieza_model->agregarPieza($data);
        redirect('odontoo/piezas/listaPieza','refresh');
        

    }
    

}