<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Odontograma extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
         $this->load->model("Odontograma_model");
  
}

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('container');
        $this->load->view('layouts/footer');
    }
    public function listaOdontograma()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');   
        $data['odontograma']=$this->Odontograma_model->retornarOdontograma();    
        $this->load->view('admin/odontograma/list',$data);
        $this->load->view('layouts/footer');
    }
    
     
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/odontograma/add');
        $this->load->view('layouts/footer');
    }

    public function agregardb()
    {
        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;
   
        
        
        $this->Odontograma_model->agregarOodntograma($data);
        redirect('odontoo/odontograma/listaOdontograma','refresh');
        

    }
    

}