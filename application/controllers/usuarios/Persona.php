<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persona extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("persona_model");
}

    public function listaPersona()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['persona']=$this->persona_model->retornarPersona();
        $this->load->view('admin/persona/list',$data);
        $this->load->view('layouts/footer');
    }
    
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/persona/add');
        $this->load->view('layouts/footer');
    }
    public function agregardb()
    {
        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;

        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;

        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;
        
        $ci=$_POST['ci'];
        $data['ci']=$ci;

        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;

        $telefono=$_POST['telefono'];
        $data['telefono']=$telefono;

        $resultado=$this->persona_model->agregarPersona($data);
        $data['resultado']=$resultado;
        redirect("usuarios/persona/listaPersona",'refresh');
            
            if($data['resultado']=$resultado){
                redirect("usuarios/persona/listaPersona",'refresh');
            }
            else{
                $this->session->set_flashdata("error","No se pudo guardar la informacion");
                redirect("usuarios/persona/agregar",'refresh');
            }
    }
    
    public function modificar()
    {
        $idPersona=$_POST['idPersona'];

        $data['nombres']=$this->persona_model->recuperarPersona($idPersona);
        $data['primerApellido']=$this->persona_model->recuperarPersona($idPersona);
        $data['segundoApellido']=$this->persona_model->recuperarPersona($idPersona);
        $data['ci']=$this->persona_model->recuperarPersona($idPersona);
        $data['direccion']=$this->persona_model->recuperarPersona($idPersona);
        $data['telefono']=$this->persona_model->recuperarPersona($idPersona);

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/persona/edit',$data);
        $this->load->view('layouts/footer');
    }
    
    public function modificardb()
    {
        $idPersona=$_POST['idPersona'];

        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;

        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;

        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;

         $ci=$_POST['ci'];
        $data['ci']=$ci;

        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;
        
        $telefono=$_POST['telefono'];
        $data['telefono']=$telefono;
        
        $this->persona_model->modificarPersona($idPersona,$data);
        redirect('usuarios/persona/listaPersona','refresh');
        
    }

    public function eliminardb()
    {
        $idPersona=$_POST['idPersona'];

        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;
        
        $primerApellido=$_POST['primerApellido'];
        $data['`primerApellido']=$primerApellido;

        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;

        $ci=$_POST['ci'];
        $data['ci']=$ci;

        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;

        $telefono=$_POST['telefono'];
        $data['telefono']=$telefono;

        // $estado=0;
        // $data['estado']=$estado;

        $this->persona_model->eliminarPersona($idPersona,$data);
        redirect('usuarios/persona/listaPersona','refresh');
    }
}