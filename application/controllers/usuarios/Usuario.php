<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("user_model");
}

    public function listaUsuario()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['usuario']=$this->user_model->retornarUsuario();
        $this->load->view('admin/usuario/list',$data);
        $this->load->view('layouts/footer');
    }
    
    public function agregar()
    {
       
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuario/add');
        $this->load->view('layouts/footer');
    }   

    public function agregardb()
    {
        $nombres=ucwords($_POST['nombres']);
        $primerApellido=ucwords($_POST['primerApellido']);
        $segundoApellido=ucwords($_POST['segundoApellido']);
        $ci=$_POST['ci'];
        $direccion=$_POST['direccion'];
        $telefono=$_POST['telefono'];
        
        $data['nombres']=$nombres;
        $data['primerApellido']=$primerApellido;
        $data['segundoApellido']=$segundoApellido;
        $data['ci']=$ci;
        $data['direccion']=$direccion;
        $data['telefono']=$telefono;  
        
        $rol=$_POST['rol'];
    

        $a=strtoupper(substr($nombres,0,1));        
        $c=$ci;

        $nombreUsuario=$a.$c;
        $password=md5($c);
        

        if($this->user_model->agregarPersona($data))
        {       
            $idPersona = $this->user_model->ultimoIDPersona();

                $data2['idUsuario']=$idPersona;
                $data2['rol']=$rol;
                $data2['nombreUsuario']=$nombreUsuario;
                $data2['password']=$password;           
               
                
                $this->user_model->guardarUsuarioo($data2);
             
            redirect("usuarios/usuario/listaUsuario",'refresh');
        }        
    }   
    
    public function modificar()
    {
        $idUsuario=$_POST['idUsuario'];

        $data['nombres']=$this->user_model->recuperarUsuario($idUsuario);
        $data['primerApellido']=$this->user_model->recuperarUsuario($idUsuario);
        $data['segundoApellido']=$this->user_model->recuperarUsuario($idUsuario);
        $data['ci']=$this->user_model->recuperarUsuario($idUsuario);
        $data['direccion']=$this->user_model->recuperarUsuario($idUsuario);
        $data['telefono']=$this->user_model->recuperarUsuario($idUsuario);
        $data['rol']=$this->user_model->recuperarUsuario($idUsuario);     


        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuario/edit',$data);
        $this->load->view('layouts/footer');
    }
    
    public function modificardb()
    {
        $idUsuario=$_POST['idUsuario'];


        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;

        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;

        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;

        $ci=$_POST['ci'];
        $data['ci']=$ci;

        $direccion=$_POST['direccion'];
        $data['direccion']=$direccion;
        
        $telefono=$_POST['telefono'];
        $data['telefono']=$telefono;

        $rol=$_POST['rol'];
        $data['rol']=$rol;
        
        $this->user_model->modificarUsuario($idUsuario,$data);
        redirect('usuarios/usuario/listaUsuario','refresh');
        
    }

    public function eliminardb()
    {
        $idUsuario=$_POST['idUsuario'];

        $nombres=$_POST['nombres'];
        $data['nombres']=$nombres;
       

        $primerApellido=$_POST['primerApellido'];
        $data['primerApellido']=$primerApellido;

        $segundoApellido=$_POST['segundoApellido'];
        $data['segundoApellido']=$segundoApellido;

        $ci=$_POST['ci'];
        $data['ci']=$ci;
         
        $rol=$_POST['rol'];
        $data['`rol']=$rol;

        $estado=0;
        $data['estado']=$estado;

        $this->user_model->eliminarUsuario($idUsuario,$data);
        redirect('usuarios/usuario/listaUsuario','refresh');
    }
}