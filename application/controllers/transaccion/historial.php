<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }  
	}

	 public function index()
	{		 
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
        $data['historiaclinica']=$this->Historial_model->retornarHistoriaClinica();
		$this->load->view('admin/historial/list',$data);
		$this->load->view('layouts/footer');
	}	
	
	 public function listahistorial()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['tratamiento']=$this->Historial_model->getTratamiento();
        $data['paciente']=$this->Historial_model->retornarPaciente();
        $this->load->view('admin/historial/add',$data);
        $this->load->view('layouts/footer');
    }

    public function getHistorial(){      
        
            $valor= $this->input->post("valor");
            $paciente= $this->Historial_model->getHistorial($valor); 
            echo json_encode($paciente);   
    }

     public function agregardb()
    {
        $fecha=$_POST['fecha'];
       //$observaciones=$_POST['observaciones'];
       // $odontograma=$_POST['odontograma'];
        $idPaciente=$_POST['idPaciente'];
 
        //$idTratamiento=$_POST['idTratamiento'];
        //$precio=$_POST['precio'];
        
        $data['fecha']=$fecha;
        $data['observaciones']='bbb';
        $data['odontograma']='bbbnnn';
        $data['idPaciente']=$idPaciente; 
    
        if($this->Historial_model->agregarHistoriall($data))
        {       
            $id= $this->Historial_model->ultimoID();
          //  $this->guardarHistoriaPacienteee($idTratamiento,$id,$precio);
             
            redirect('transaccion/historial','refresh');
        }
        else{
            redirect('transaccion/historial','refresh');
        }
    }

    // protected function guardarHistoriaPacienteee($idTratamiento,$idHistoriaClinica,$precio){
    //     for ($i=0; $i < count($precio); $i++) { 
    //             $data['idTratamiento']=$idTratamiento;
    //             $data['idHistoriaClinica']=$idHistoriaClinica[$i];
    //             $data['precio']=$precio[$i];
              
    //             $this->Historial_model->guardarHistorialPaciente($data);
    //     }

    }
  


