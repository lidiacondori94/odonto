<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pacientee extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("paciente_model");
}

    public function listaPaciente()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['paciente']=$this->paciente_model->retornarPaciente();
        $this->load->view('admin/paciente/list',$data);
        $this->load->view('layouts/footer');
    }
    
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/paciente/add');
        $this->load->view('layouts/footer');
    }
    public function agregardb()
    {
        $nombres=ucwords($_POST['nombres']);
        $primerApellido=ucwords($_POST['primerApellido']);
        $segundoApellido=ucwords($_POST['segundoApellido']);
        $ci=$_POST['ci'];
        $direccion=$_POST['direccion'];
        $telefono=$_POST['telefono'];
        
        $data['nombres']=$nombres;
        $data['primerApellido']=$primerApellido;
        $data['segundoApellido']=$segundoApellido;
        $data['ci']=$ci;
        $data['direccion']=$direccion;
        $data['telefono']=$telefono; 


        $fechaNacimiento=$_POST['fechaNacimiento'];
        

        if($this->paciente_model->agregarPersona($data))
        {       
            $idPersona = $this->paciente_model->ultimoIDPersona();

                $data2['idPaciente']=$idPersona;
                $data2['fechaNacimiento']=$fechaNacimiento;
                         
               
                
                $this->paciente_model->guardarPacientee($data2);
             
            redirect("paciente/pacientee/listaPaciente",'refresh');
        }        
    }   
    
    public function modificar()
    {
        $idPaciente=$_POST['idPaciente'];

        $data['fechaNacimiento']=$this->paciente_model->recuperarPacientee($idPaciente);
      

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/paciente/edit',$data);
        $this->load->view('layouts/footer');
    }
    
    public function modificardb()
    {
        $idPaciente=$_POST['idPaciente'];

        $fechaNacimiento=$_POST['fechaNacimiento'];
        $data['fechaNacimiento']=$fechaNacimiento;
       
        
        $this->paciente_model->modificarPaciente($idPaciente,$data);
        redirect('paciente/pacientee/listaPaciente','refresh');
        
    }

    public function eliminardb()
    {
        $idPaciente=$_POST['idPaciente'];

        $fechaNacimiento=$_POST['fechaNacimiento'];
        $data['fechaNacimiento']=$fechaNacimiento;      
       

        $estado=0;
        $data['estado']=$estado;

        $this->paciente_model->eliminarPaciente($idPaciente,$data);
        redirect('paciente/pacientee/listaPaciente','refresh');
    }
}