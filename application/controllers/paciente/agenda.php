<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("agenda_model");
}

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('container');
        $this->load->view('layouts/footer');
    }
    public function listaAgenda()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['agendacitas']=$this->agenda_model->retornarAgenda();
        $this->load->view('admin/agenda/list',$data);
        $this->load->view('layouts/footer');
    }
    
     
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/agenda/add');
        $this->load->view('layouts/footer');
    }

    public function agregardb()
    {
        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;

        $hora=$_POST['hora'];
        $data['hora']=$hora;
        
        // $idUsuario=$_POST['idUsuario'];
        // $data['idUsuario']=$idUsuario;

        // $idPaciente=$_POST['idPaciente'];
        // $data['idPaciente']=$idPaciente;
        
        
        $this->agenda_model->agregarAgenda($data);
        redirect('paciente/agenda/listaAgenda','refresh');
        
    }
    public function modificar()
    {
        $idAgenda=$_POST['idAgenda'];

        $data['fecha']=$this->agenda_model->recuperarTratamiento($idAgenda);
        $data['hora']=$this->agenda_model->recuperarTratamiento($idAgenda);           


        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/agenda/edit',$data);
        $this->load->view('layouts/footer');
    }




    public function modificardb()
    {
        $idAgenda=$_POST['idAgenda'];

        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;
        
        $hora=$_POST['hora'];
        $data['hora']=$hora;
        
        
        $this->agenda_model->modificarTratamiento($idTratamiento,$data);
        redirect('paciente/tratamiento/listaAgenda','refresh');
        

        
        
    }

    public function eliminardb()
    {
        $idAgenda=$_POST['idAgenda'];

        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;
        
        $hora=$_POST['hora'];
        $data['hora']=$hora;
        

        $this->agenda_model->eliminarAgenda($idAgenda,$data);
        redirect('paciente/agenda/listaAgenda','refresh');
        //$this->load->view('head');
        //$this->load->view('eliminarmensaje',$data);
        //$this->load->view('footer');

    }
}