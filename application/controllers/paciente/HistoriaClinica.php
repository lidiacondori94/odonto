<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HistoriaClinica extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("historiaclinica_model");
}

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('container');
        $this->load->view('layouts/footer');
    }
    public function listaHistoriaClinica()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['historiaClinica']=$this->historiaclinica_model->retornarHistoria();
        $this->load->view('admin/historiaclinica/list',$data);
        $this->load->view('layouts/footer');
    }
    
     
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/historiaclinica/add');
        $this->load->view('layouts/footer');
    }

    public function agregardb()
    {
        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;

        $observaciones=$_POST['observaciones'];
        $data['observaciones']=$observaciones;  


        $odontograma=1;
        $data['odontograma']=$odontograma;  

         $idPaciente=1;
        $data['idPaciente']=$idPaciente;       

       
        
  
        $this->historiaclinica_model->agregarHistoria($data);
        redirect('paciente/historiaclinica/listaHistoriaClinica','refresh');
        


    }
    public function modificar()
    {
        $idHistoriaClinica=$_POST['idHistoriaClinica'];

        $data['fecha']=$this->historiaclinica_model->recuperarHistoria($idHistoriaClinica);
        $data['observaciones']=$this->historiaclinica_model->recuperarHistoria($idHistoriaClinica);
        $data['odontograma']=$this->historiaclinica_model->recuperarHistoria($idHistoriaClinica);
        $data['idPaciente']=$this->historiaclinica_model->recuperarHistoria($idHistoriaClinica);
            


        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/historiaclinica/edit',$data);
        $this->load->view('layouts/footer');
    }




    public function modificardb()
    {
        $idHistoriaClinica=$_POST['idHistoriaClinica'];

        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;
        
        $observaciones=$_POST['observaciones'];
        $data['observaciones']=$observaciones;
        
        $odontograma=$_POST['odontograma'];
        $data['odontograma']=$odontograma;
        
    

        $this->historiaclinica_model->modificarHistoria($idHistoriaClinica,$data);
        redirect('paciente/historiaclinica/listaHistoriaClinica','refresh');
        
        
        
    }

    public function eliminardb()
    {
        $idHistoriaClinica=$_POST['idHistoriaClinica'];

        $fecha=$_POST['fecha'];
        $data['fecha']=$fecha;
        
        $observaciones=$_POST['observaciones'];
        $data['observaciones']=$observaciones;
        
        // $odontograma=$_POST['odontograma'];
        // $data['odontograma']=$odontograma;
        

        $this->historiaclinica_model->eliminarHistoria($idHistoriaClinica,$data);
        redirect('paciente/historiaclinica/listaHistoriaClinica','refresh');       

    }
}