<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historia extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("Historia_model");
    
    $this->load->model("Paciente_model");

}

public function index()
	{
		$data = array(
            'historClinica' => $this->Historia_model->getHistoria(),
        );  
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/historiaclinica/list',$data);
		$this->load->view('layouts/footer');

	}

public function add(){

    $data = array(
        
        "paciente" => $this->Paciente_model->getPaciente()
    );

    $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/historiaclinica/add',$data);
		$this->load->view('layouts/footer');

}

public function store(){

        $fecha = $this->input->post("fecha");
        $observaciones = $this->input->post("observaciones");
        $odontograma = $this->input->post("odontograma");
        $pacientes = $this->input->post("pacientes");       

        $this->form_validation->set_rules("fecha","Fecha","required|is_unique[historiaclinica.fecha]");
        $this->form_validation->set_rules("observaciones","Observaciones","required");
        $this->form_validation->set_rules("odontograma","odontograma","required");
       

        if ($this->form_validation->run()) {

             $data = array(
            'fecha' => $fecha,
            'observaciones' => $observaciones,
            'odontograma' => $odontograma,            
            'idPaciente' => $pacientes,         
            
            );

        if($this->Historia_model->save($data)){
            redirect(base_url()."paciente/historia");
        }
        else{
            $this->session->set_flashdata("error","No se pudo guardar la informacion");
            redirect(base_url()."paciente/historia/add");
        }

    }
    else{
        $this->add();
    }
}


public function edit($id){
    $data = array(
        "historiaClinica" =>$this->Historia_model->getHistoria($id),
        //crear paciente
        "paciente" => $this->Paciente_model->getPaciente()
    );
        $this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/historiaclinica/edit',$data);
		$this->load->view('layouts/footer');
}
public function update(){
        $idHistoriaClinica = $this->input->post("idHistoriaClinica");
        $fecha = $this->input->post("fecha");
        $observaciones = $this->input->post("observaciones");
        $odontograma = $this->input->post("odontograma");     
        $pacientes = $this->input->post("pacientes");

        $historiaoActual = $this->Historia_model->getHistoria($idHistoriaClinica);
        if ($fecha == $historiaActual->fecha) {
            $is_unique = '';
        } 
        else{
            $is_unique = '|is_unique[historiaClinica.fecha]';
        }
        


        $this->form_validation->set_rules("fecha","Fecha","required".$is_unique);
        $this->form_validation->set_rules("observaciones","Observaciones","required");
        $this->form_validation->set_rules("odontograma","Odontograma","required");
       
        if ($this->form_validation->run()) {
                $data = array(
            'fecha' => $fecha,
            'observaciones' => $observaciones,
            'odontograma' => $odontograma,   
            'idPaciente' => $pacientes,
            
            
            );
            if ($this->Historia_model->update($idHistoriaClinica,$data)) {
                redirect(base_url()."paciente/historia");
            } else {
               $this->session->set_flashdata("error","No se pudo guardar la informacion");
            redirect(base_url()."paciente/historia/edit/".$idHistoriaClinica);
            }
        } else {
            $this->edit($idHistoriaClinica);
        }     
            

}


  public function delete($id){
        // $data = array(
        //     'estado' => "0",
        //     );
            $this->Historia_model->update($idHistoriaClinica, $data);
            echo "paciente/historia";

    }
}