<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tratamiento extends CI_Controller {

    public function __construct(){
    parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url());
        }
  
    $this->load->model("tratamiento_model");
}

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('container');
        $this->load->view('layouts/footer');
    }
    public function listaTratamiento()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $data['tratamiento']=$this->tratamiento_model->retornarTratamiento();
        $this->load->view('admin/tratamientos/list',$data);
        $this->load->view('layouts/footer');
    }
    
     
    public function agregar()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/tratamientos/add');
        $this->load->view('layouts/footer');
    }

    public function agregardb()
    {
        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;

        $descripcion=$_POST['descripcion'];
        $data['descripcion']=$descripcion;
        
        $precio=$_POST['precio'];
        $data['precio']=$precio;
        
        
        $this->tratamiento_model->agregarTratamiento($data);
        redirect('paciente/tratamiento/listaTratamiento','refresh');
        

    }
    public function modificar()
    {
        $idTratamiento=$_POST['idTratamiento'];

        $data['nombre']=$this->tratamiento_model->recuperarTratamiento($idTratamiento);
        $data['descripcion']=$this->tratamiento_model->recuperarTratamiento($idTratamiento);
        $data['precio']=$this->tratamiento_model->recuperarTratamiento($idTratamiento);


        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/tratamientos/edit',$data);
        $this->load->view('layouts/footer');
    }




    public function modificardb()
    {
        $idTratamiento=$_POST['idTratamiento'];

        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;
        
        $descripcion=$_POST['descripcion'];
        $data['descripcion']=$descripcion;
        
        $precio=$_POST['precio'];
        $data['precio']=$precio;


        // $data =array( 
        //     "tratamientos" => $this->tratamiento_model->getTratamiento($idTratamiento)
            
        // );           
    

        $this->tratamiento_model->modificarTratamiento($idTratamiento,$data);
        redirect('paciente/tratamiento/listaTratamiento','refresh');
        

        
        
    }

    public function eliminardb()
    {
        $idTratamiento=$_POST['idTratamiento'];

        $nombre=$_POST['nombre'];
        $data['nombre']=$nombre;
        
        $descripcion=$_POST['descripcion'];
        $data['descripcion']=$descripcion;
        
        $precio=$_POST['precio'];
        $data['precio']=$precio;
        

        $this->tratamiento_model->eliminarTratamiento($idTratamiento,$data);
        redirect('paciente/tratamiento/listaTratamiento','refresh');
        //$this->load->view('head');
        //$this->load->view('eliminarmensaje',$data);
        //$this->load->view('footer');

    }
}