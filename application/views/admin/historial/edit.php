
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Historial        
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                               <div class="form-group">
                                    <label for="">fecha de historia:</label>
                                    <div class="input-group">
                                        <input type="date" id="idPaciente" name="idPaciente">                            
                                          <span class="input-group-btn">
                                         
                                                <span class="fa fa-search"></span>
                                            </button>
                                          </span>
                                    </div><!-- /input-group -->
                                  </div>
                               </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Seleccione Paciente:</label>
                                    <div class="input-group">
                                        <input type="hidden" id="idPaciente" name="idPaciente">
                                        <input type="text" class="form-control" disabled="disabled" name="infoCliente" id="infoCliente">
                                          <span class="input-group-btn">
                                            <button class="btn btn-primary btn-flat" type="button" data-toggle="modal" data-target="#modal-clientes">
                                                <span class="fa fa-search"></span>
                                            </button>

                                          </span>
                                    </div><!-- /input-group -->
                                </div>
                            </div>

                        </div>
                           
                        <div class="form-group">
                          
                            <form action="<?php echo base_url();?>transaccion/historial/agregar" method="POST" id="add-orden">
                            
                                <h4 class="text-center">Tratamientos Agregados al Paciente</h4>
                                <div class="table-responsive">
                                <table class="table table-hover table-bordered" id="tborden">
                                    <thead>
                                        <tr>
                                            <th>Tratamientos</th>
                                            <th>Precio por Tratamiento</th>
                                            <th>Cantidad</th>
                                            <th>Importe</th>
                                            <th>Descuento</th>
                                            <th>TotalPago</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="message">
                                            <td colspan="5" class="text-center">Aun no se han agregado tratamiento</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="3" class="text-right">Pago Total</th>
                                            <td>
                                                <input type="hidden" name="pagoTotal" value="0">
                                                <p class="pagoTotal">0.00</p>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                </div>
                                <div class="form-group">
                                    <button id="btn-success" type="submit" class="btn btn-success btn-flat btn-guardar" disabled="disabled">Guardar</button>
                                    <a href="<?php echo base_url();?>trasaccion/historial" class="btn btn-danger">Volver</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Seleccion de tratamiento</h4>
                        <div class="form-group">
                            <select name="tratamiento" id="tratamiento" class="form-control">
                                <option value="">Seleccione Tratamiento</option>
                                <!-- <?php foreach ($tratamientos as $tratamiento): ?>
                                    <option value="<?php echo $tratamiento->id;?>"><?php echo $tratamiento->nombre;?></option>
                                <?php endforeach ?> -->
                            </select>
                            <label>Odontograma</label>
                            <span class="input-group-btn">
                                            <button class="btn btn-primary btn-flat" type="button" data-toggle="modal" data-target="#modal-clientes">
                                                <span class="fa fa-search"></span>
                        </div>
                        <table class="table table-bordered table-hover" id="tbproductos">
                            <thead>
                                <tr>
                                    
                                    <th>Tratamiento</th>
                                    <th>Seleccionar</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            
                        </table>

                        
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-venta">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Informacion de la Historia</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button id="btn-cmodal" type="button" class="btn btn-danger pull-left btn-cerrar-imp" data-dismiss="modal">Cerrar</button>
    <!-- <button type="button" class="btn btn-primary btn-flat btn-print"><span class="fa fa-print"></span> Imprimir</button> -->
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-clientes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Listado de Clientes</h4>
            </div>
            <div class="modal-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Documento</th>
                            <th>Opcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($clientes)):?>
                            <?php foreach($clientes as $cliente):?>
                                <tr>
                                    <td><?php echo $cliente->id;?></td>
                                    <td><?php echo $cliente->nombre;?></td>
                                    <td><?php echo $cliente->num_documento;?></td>
                                    <?php $datacliente = $cliente->id."*".$cliente->nombre."*".$cliente->tipocliente."*".$cliente->tipodocumento."*".$cliente->num_documento."*".$cliente->telefono."*".$cliente->direccion;?>
                                    <td>
                                        <button type="button" class="btn btn-success btn-check" value="<?php echo $datacliente;?>"><span class="fa fa-check"></span></button>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="btn-cmodal" type="button" class="btn btn-danger pull-left btn-cerrar-imp" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary btn-flat btn-print"><span class="fa fa-print"></span> Imprimir</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
