
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <center><h2>
        Historial Nuevo        
        </h2></center>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="<?php echo base_url();?>transaccion/historial/agregardb" method="POST" class="form-horizontal">
                                
                        <div class="col-md-3">        
                                  <div class="col-md-12">    
                                    <div>                                                      
                                    <center><label>Odontograma</label></center>  
                                                           
                                    <center><button class="btn btn-primary btn-flat" type="button" > <a href="<?php echo base_url();?>odontogra/odontogram">
                                    <span class="fa fa-search"></span></a>
                                    </button></center>
                                   </div> 
                                   </div>                                   
<br>
                                <div class="col-md-12">
                                   <label for="">Fecha:</label>
                                    <input type="date" class="form-control" name="fecha" required>
                                </div>

                            <br>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Paciente:</label>
                                    <div class="input-group">
                                        <input type="hidden" name="idPaciente" id="idPaciente">
                                        <input type="text" class="form-control" disabled="disabled" id="paciente">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-default" ><span class="fa fa-search"></span> Buscar</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div> 
                             </div>

<br>

                                 <div class="col-md-12">
                                <div class=form-group>
                                 <label for="tratamient">Tratamiento: </label>
                                  <input type="text" class="form-control" id="tratamiento">         
                                  
                               </div > 
                               </div>  

                                <br>
                               <div class="col-md-12">
                                      <button id="btn-agregar" type="button" class="btn btn-success btn-flat btn-block"><span class="fa fa-plus"></span> Agregar</button>
                                </div> 

                        </div>  
                        <div class="col-md-9"> 
                                <div class="col-md-12">
                                      <img src="<?=base_url()?>assets/template/img/8.png" class="img-responsive" alt="First slide">
                                </div> 
                        </div>   


                            <br>    
                                
                            <table id="tbTratamiento" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                            <th>Tratamientos</th>
                                            <th>Numero Pieza</th>
                                            <th>Precio Tratamiento</th>
                                            <th>Cantidad de tratamientos</th>                                           
                                            <th>Total Pago</th>
                                            <th>Opcion</th>
                                        </tr>
                                </thead>
                                <tbody>                                 
                                                        
                                
                                </tbody>
                            </table>
                           
                            <div class="form-group">
                                <br>
                                <br>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                                    <a href="<?php echo base_url();?>transaccion/historial" class="btn btn-primary">Volver</a>
                                </div>           
                                
                            </div>


                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lita de Pacientes</h4>
            </div>
            <div class="modal-body">
                <table id="example1" class="table table-bordered table-striped table-hover">
                   <thead>
                        <tr>
                            <th>#</th>
                            <th>NOMBRE PACIENTE</th>
                            <th>CI</th>
                            <th>OPCIÓN</th>
                        </tr>
                    </thead>
                     <tbody>
                         <?php
                        $indice=1;
                        foreach ($paciente->result() as $row) {
                        ?>
                    <tr>
                    <td> <?php echo $indice;?></td>
                    <td><?php echo $row->nombres.' '.$row->primerApellido.' '.$row->segundoApellido;?></td>
                    <td><?php echo $row->ci;?></td>
                   
                    <?php 
                     $datapaciente = $row->idPersona."*".$row->nombres."*".$row->primerApellido."*".$row->segundoApellido."*".$row->ci;?>

                    <td>
              <button type="button" class="btn btn-success btn-check" value="<?php echo $datapaciente;?>">
              <span class="fa fa-check"></span></button>
          </td>
                    </tr>

                    
                    <?php
                      $indice++;
                      }
                      ?>   
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


