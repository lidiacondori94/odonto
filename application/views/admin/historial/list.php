
<!-- Content Wrapper. Contains page content -->
 <style type="">
       .cabecera
       {
        background-color: #1790BC;
       }
   </style> 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <center><h2>
        HISTORIAL         
        </h2></center>
  
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo base_url();?>transaccion/historial/listahistorial" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Historia</a>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr class="cabecera">
                                    <th>#</th>
                                    <th>PACIENTE</th>
                                    <th>FECHA H.CLINICA</th>
                                    <th>ODONTOGRAMA</th>
                                    <th>OBSERVACIONES</th>                                  
                                                                    
                                    <th>OPCIONES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($historiaclinica->result() as $row) {
                                ?>
                                        <tr>
                                             <td><?php echo $indice;?></td>
                                            <td><?php echo $row->idPaciente;?></td>
                                            <td><?php echo $row->fecha;?></td>
                                            <td><?php echo $row->odontograma;?></td>
                                            <td><?php echo $row->observaciones;?></td>
                                           
                                           
                                        </tr>
                                   <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Informacion del Historial</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Cerrar</button>
       <!--  <button type="button" class="btn btn-primary btn-print"><span class="fa fa-print"> </span>Imprimir</button> -->
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
