 <style type="">
            .cruds{
             background-color: #33BC57;
             }
        </style>
        <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                LISTA DE PERSONAS
                </h1>
            </section>
            <section class="content-header">
                <?php echo form_open_multipart('usuarios/persona/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>

                <!-- <a href="<?=base_url()?>admin/listaUsuarioPdf" class="btn btn-oval btn-danger" type="submit">Exportar a PDF <i class="fa fa-file-pdf-o "></i></a>
                <?php echo form_close(); ?> -->
            </section>
            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="example1">
                            
                            <thead class="thead-inverse">
                                <tr class="cruds">
                                <th>#</th>
                                <th>NOMBRE COMPLETO</th>                                
                                <th>CI</th>
                                <th>DIRECCION</th>
                                <th>TELEFONO</th>
                                <th>OPCIONES</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($persona->result() as $row) {
                                ?>
                                    <tr>
                                        <td><?php echo $indice; ?></td>
                                        <td><?php echo $row->nombres.' '.$row->primerApellido.' '.$row->segundoApellido; ?></td>                                        
                                        <td><?php echo $row->ci; ?></td>
                                        <td><?php echo $row->direccion; ?></td>
                                        <td><?php echo $row->telefono; ?></td>
                                        
                                        <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('usuarios/persona/modificar'); ?>
                                                <input type="hidden" name="idPersona" value="<?php echo $row->idPersona; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('usuarios/persona/eliminardb'); ?>
                                                    <input type="hidden" name="idPersona" value="<?php echo $row->idPersona; ?>">
                                                    <input type="hidden" name="nombres" value="<?php echo $row->nombres; ?>">
                                                    <input type="hidden" name="primerApellido" value="<?php echo $row->primerApellido; ?>">
                                                    <input type="hidden" name="segundoApellido" value="<?php echo $row->segundoApellido; ?>">
                                                    <input type="hidden" name="ci" value="<?php echo $row->ci; ?>">
                                                    <input type="hidden" name="direccion" value="<?php echo $row->direccion; ?>">
                                                    <input type="hidden" name="telefono" value="<?php echo $row->telefono; ?>">
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>
       

