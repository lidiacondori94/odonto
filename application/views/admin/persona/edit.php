
   
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                  MODIFICAR PERSONA
                </h1> 
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                foreach ($nombres->result() as $row) {
                                ?>
                                <?php echo form_open_multipart('usuarios/persona/modificardb'); ?>
                            
                                <form action="/action_page.php">
                                    <div class=form-group>
                                    <input type="hidden" name="idPersona" value="<?php echo $row->idPersona; ?>">
                                     </div >
                                    <div class=form-group>
                                        <label for="nombres">Nombres: </label>
                                        <input type="text" name="nombres" class="form-control" id="nombres" value="<?php echo $row->nombres; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >        
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido: </label>
                                        <input type="text" name="primerApellido" class="form-control" id="primerApellido" value="<?php echo $row->primerApellido; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >        
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido: </label>
                                        <input type="text" name="segundoApellido" class="form-control" id="segundoApellido" value="<?php echo $row->segundoApellido; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >        
                                    <div class=form-group>
                                        <label for="ci">CI: </label>
                                        <input type="text" name="ci" class="form-control" id="ci" value="<?php echo $row->ci; ?>" pattern="[0-9\s]+" title="Solo puede contener caracteres alfavetico.">
                                    </div >     
                                    <div class=form-group>
                                        <label for="direccion">Direccion: </label>
                                        <input type="text" name="direccion" class="form-control" id="direccion" value="<?php echo $row->direccion; ?>"pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                    </div >     
                                    <div class=form-group>
                                        <label for="telefono"> Telefono: </label>
                                        <input type="text" name="telefono" class="form-control" id="telefono" value="<?php echo $row->telefono; ?>" pattern="[0-9\s]+" title="Solo puede contener caracteres alfavetico.">
                                    </div >         
                                    <hr>                   
                                    <div class=form-group>
                                    
                                        <button type="submit" class="btn btn-oval btn-primary pull-center">Registrar</button>
                                        <?php echo form_close(); ?>
                                    
                                    <a href="<?=base_url()?>index.php/usuarios/persona/listaPersona" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div >  
                                          
                                    <?php
                                    }
                                    ?>
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       
