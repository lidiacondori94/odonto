        <div class="content-wrapper">
            
            <section class="content-header">
                <h1>
                REGISTRO DE UN NUEVO PACIENTE
                </h1>
            </section>
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/usuarios/persona/listaPersona">Ir atras</a>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                 <?php echo form_open_multipart('usuarios/persona/agregardb'); ?>
                                
                                 <div>
                                      
                                    <form method="POST">
                                    
                                        <div class="form-group">
                                            <label for="nombres">Nombres: </label>
                                            <input type="text" name="nombres" placeholder="Ingrese su nombre.."class="form-control" id="nombres" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                         <div class="form-group">
                                            <label for="primerApellido">Primer Apellido: </label>
                                            <input type="text" name="primerApellido" placeholder="Ingrese su primer apellido.."class="form-control" id="primerApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                         <div class="form-group">
                                            <label for="segundoApellido">Segundo Apellido: </label>
                                            <input type="text" name="segundoApellido" placeholder="Ingrese su segundo apellido.."class="form-control" id="segundoApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                           <div class=form-group>
                                            <label for="ci">CI: </label>
                                            <input type="text" name="ci" placeholder="Ingrese su CI.."class="form-control" id="ci" pattern="[0-9\s]+" title="Solo puede contener caracteres numéricos." >
                                        </div > 
                                        <div class="form-group">
                                            <label for="direccion">Direccion: </label>
                                            <input type="text" name="direccion" placeholder="Ingrese su direccion .."class="form-control" id="direccion" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >

                                        <div class=form-group>
                                            <label for="telefono">Telefono: </label>
                                            <input type="text" name="telefono" placeholder="Ingrese el numero de telefono.."class="form-control" id="telefono" pattern="[0-9\s]+" title="Solo puede contener caracteres numéricos." >
                                        </div >                
                                        <div>
                    
                                            <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                            <?php echo form_close(); ?>

                                            <a href="<?=base_url()?>index.php/usuarios/persona/listaPersona" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                        </div >  
                                        
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       





