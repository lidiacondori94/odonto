 <style type="">
            .cruds{
             background-color: #33BC57;
             }
        </style>
        <div class="content-wrapper">
            
            <section class="content-header">
                <br>
                <h1 class="text-center">
                LISTA DE PACIENTES
                </h1>
            </section>
            <section class="content-header">
                <?php echo form_open_multipart('paciente/pacientee/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>
            </section>
            <!-- Main content -->

            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="example1">
                            
                            <thead class="thead-inverse">
                                <tr class="cruds">
                                <th>#</th>
                                <th>NOMBRE COMPLETO</th>
                                <th>CI</th>                                
                                <th>FECHA DE NACIMIENTO</th>
                                                              
                                <th>OPCIONES</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($paciente->result() as $row) {
                                ?>
                                    <tr>
                                         <td><?php echo $indice; ?></td>
                                        <td><?php echo $row->nombres.' '.$row->primerApellido.' '.$row->segundoApellido; ?></td>
                                        <td><?php echo $row->ci; ?></td> 
                                        <td><?php echo $row->fechaNacimiento; ?></td> 
                                        
                                        
                                        <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/pacientee/modificar'); ?>
                                                <input type="hidden" name="idPaciente" value="<?php echo $row->idPaciente; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/pacientee/eliminardb'); ?>
                                                    <input type="hidden" name="idPaciente" value="<?php echo $row->idPaciente; ?>">
                                                    <input type="hidden" name="fechaNacimiento" value="<?php echo $row->fechaNacimiento; ?>">
                                                    <input type="hidden" name="fechaRegistro" value="<?php echo $row->fechaRegistro; ?>">
                                                    
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                    
                                                <?php echo form_close(); ?>

                                            </div>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/historiaclinica/agregar'); ?>
                                               <button type="submit" class="btn btn-oval btn-primary"> <i class="fa fa-book "></i></button>
                                               <?php echo form_close(); ?>
                                               </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>
       

