 <style type="">
            .cruds{
             background-color: #33BC57;
             }
        </style>
        <div class="content-wrapper">
          
            <section class="content-header">
                <br>
                 <h1 class="text-center">
                LISTA DE HISTORIA CLINICA
                </h1>
            </section>       
             <section class="content-header">
                <?php echo form_open_multipart('paciente/historiaclinica/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>
                <a href= "<?php echo base_url();?>transaccion/historial" type="button" class="btn btn-oval btn-primary">Historial</a>

               
            </section>     

             <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="example1">
                            
                            <thead class="thead-inverse">
                                 <tr class="cruds">
                                    <th>No.</th>
                                    <th>FECHA</th>
                                    <th>OBSERVACIONES</th>                                   
                                    <th>PACIENTE</th>
                                    <th>OPCIONES</th>                                                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($historiaClinica->result() as $row) {
                                ?>
                                    <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->fecha; ?></td>
                                    <td><?php echo $row->observaciones; ?></td>                                    
                                    <!-- <td><?php echo $row->odontograma; ?><input type="button" onclick=" location='odontograma.php'" />
                                    </td> -->
                                    <td><?php echo $row->nombres.' '.$row->primerApellido.' '.$row->segundoApellido; ?></td>
                                                                       
                                    <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/historiaclinica/modificar'); ?>
                                                <input type="hidden" name="idHistoriaClinica" value="<?php echo $row->idHistoriaClinica; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/historiaclinica/eliminardb'); ?>
                                                    <input type="hidden" name="idHistoriaClinica" value="<?php echo $row->idHistoriaClinica; ?>">
                                                    <input type="hidden" name="fecha" value="<?php echo $row->fecha; ?>"></input>
                                                    <input type="hidden" name="observaciones" value="<?php echo $row->observaciones; ?>"></input>
                                                   
                                                    <input type="hidden" name="idPaciente" value="<?php echo $row->idPaciente; ?>"></input>
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                            
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>       
                         
                                      
                                            
                                        