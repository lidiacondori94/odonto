 <style type="">
            .cruds{
             background-color: #33BC57;
             }
        </style>
        <div class="content-wrapper">
          
            <section class="content-header">
                <br>
                 <h1 class="text-center">
                LISTA DE TRATAMIENTOS
                </h1>
            </section>       
             <section class="content-header">
                <?php echo form_open_multipart('paciente/tratamiento/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>

                <!-- <a href="<?=base_url()?>admin/listaUsuarioPdf" class="btn btn-oval btn-danger" type="submit">Exportar a PDF <i class="fa fa-file-pdf-o "></i></a>
                <?php echo form_close(); ?> -->
            </section>     

             <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="example1">
                            
                            <thead class="thead-inverse">
                                 <tr class="cruds">
                                    <th>No.</th>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Precio</th>
                                    <th>Opciones</th>                                                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($tratamiento->result() as $row) {
                                ?>
                                    <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->nombre; ?></td>
                                    <td><?php echo $row->descripcion; ?></td>
                                    <td><?php echo $row->precio; ?></td>
                                                                       
                                    <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/tratamiento/modificar'); ?>
                                                <input type="hidden" name="idTratamiento" value="<?php echo $row->idTratamiento; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/tratamiento/eliminardb'); ?>
                                                    <input type="hidden" name="idTratamiento" value="<?php echo $row->idTratamiento; ?>">
                                                    <input type="hidden" name="nombre" value="<?php echo $row->nombre; ?>"></input>
                                                    <input type="hidden" name="descripcion" value="<?php echo $row->descripcion; ?>"></input>
                                                    <input type="hidden" name="precio" value="<?php echo $row->precio; ?>"></input>
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>       
                         
                                      
                                            
                                        