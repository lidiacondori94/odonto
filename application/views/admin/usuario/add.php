        <div class="content-wrapper">
            <section class="content-header">
                <a href="<?php echo base_url();?>index.php/usuarios/usuario/listaUsuario">Ir atras</a>
            </section>
            <section class="content-header">
                <h1>
                REGISTRO DE NUEVO USUARIO
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-12">

                                <?php if($this->session->flashdata("error")):?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata("error");?></p>
                                </div>
                                <?php endif;?>

                                 <?php echo form_open_multipart('usuarios/usuario/agregardb'); ?>
                                
                                 <div>
                                <form>
                                    <br>
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="nombres">Nombre: </label>
                                        <input type="text" name="nombres" placeholder="Ingrese nombre..."class="form-control" id="nombres" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido : </label>
                                        <input type="text" name="primerApellido" placeholder="Ingrese el primer apellido ...."class="form-control" id="primerApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >                                

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido : </label>
                                        <input type="text" name="segundoApellido" placeholder="Indrese el segundo apellido.."class="form-control" id="segundoApellido" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" title="Solo puede contener caracteres alfabeticos." >
                                    </div > 
                                    </div >
                                    <br>

                                    
                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="ci">Carnet de Identidad: </label>
                                        <input type="text" name="ci" placeholder="Ingrese su número de identificacion C.I..."class="form-control" id="ci" pattern="[A-Za-z0-9-\s]+" required="" title="Solo puede contener caracteres alfa númericos." >
                                    </div > 
                                    </div >

                                    <div class="col-md-4">


                                     <div class="form-group">
                                            <label for="direccion">Direccion: </label>
                                            <input type="text" name="direccion" placeholder="Escriba la direccion ..."class="form-control" id="direccion" pattern="^[A-Za-zñÑáéíóúÁÉÍÓÚ]+[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+[A-Za-zñÑáéíóúÁÉÍÓÚ]$" required="" title="Solo puede contener caracteres alfavetico.">
                                        </div >
                                    </div>

                                    <div class="col-md-4">
                                    <div class=form-group>
                                        <label for="telefono">Telefono/Celular: </label>
                                        <input type="text" name="telefono" placeholder="Ingrese número de tel-cel..."class="form-control" id="telefono" pattern="[0-9\s]+" required="" title="Solo puede contener caracteres númericos" >
                                    </div > 
                                    </div >                                

                                     <div class="col-md-4">
                                     <div class="form-group">
                                            <label for="rol">Rol: </label>
                                            <!-- <input type="text" name="rol" id="rol"> -->
                                            <select name="rol">
                                            <option value="1">.......</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Odontologo">Odontologo</option>
                                            <option value="Recepcionista">Recepcionista</option>
                                            </select>
                                        </div>
                                        </div >
                               
                                    
                                   
                                    
                                    <div class="col-md-12">
                                    <br>
                                    <br>
                                    <div>
                
                                        <button type="submit" class="btn btn-oval btn-primary">Registrar</button>
                                        <?php echo form_close(); ?>

                                        <a href="<?=base_url()?>index.php/usuarios/listaUsuario" class="btn btn-oval btn-danger" type="submit">Cancelar</a>
                                    </div >                                         
                                        
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       





