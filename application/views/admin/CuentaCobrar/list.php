 <style type="">
            .cruds{
             background-color: #33BC57;
             }
        </style>
        <div class="content-wrapper">
          
            <section class="content-header">
                <br>
                 <h1 class="text-center">
                LISTA DE CUENTAS
                </h1>
            </section>       
             <section class="content-header">
                <?php echo form_open_multipart('paciente/cuentacobrar/agregar'); ?>
                <button type="submit" class="btn btn-oval btn-primary">Agregar <i class="fa fa-user "></i></button>
            </section>     

             <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        
                        <table class="table table-striped table-inverse table-responsive" id="example1">
                            
                            <thead class="thead-inverse">
                                 <tr class="cruds">
                                    <th>No.</th>
                                    <th>PAGO TOTAL</th>
                                    <th>DESCUENTO</th>
                                    <th>TRATAMIENTO</th>
                                    <th>PACIENTE</th>
                                    <th>OPCIONES</th>                                                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $indice=1;
                                foreach ($cuentaporcobrar->result() as $row) {
                                ?>
                                    <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->pagoTotal; ?></td>
                                    <td><?php echo $row->descuento; ?></td>
                                    <td><?php echo $row->idTratamiento; ?></td>
                                    <td><?php echo $row->idPaciente; ?></td>
                                                                       
                                    <td>
                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/cuentacobrar/modificar'); ?>
                                                <input type="hidden" name="idCuentaPorCobrar" value="<?php echo $row->idCuentaPorCobrar; ?>">
                                                <button class="btn btn-warning  " type="submit" name="action"><i class="fa fa-pencil"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>

                                            <div class="btn-group">
                                                <?php echo form_open_multipart('paciente/cuentacobrar/eliminardb'); ?>
                                                    <input type="hidden" name="idCuentaPorCobrar" value="<?php echo $row->idCuentaPorCobrar; ?>">
                                                    <input type="hidden" name="pagoTotal" value="<?php echo $row->pagoTotal; ?>"></input>
                                                    <input type="hidden" name="descuento" value="<?php echo $row->descuento; ?>"></input>
                                                    <input type="hidden" name="idTratamiento" value="<?php echo $row->idTratamiento; ?>"></input>
                                                    <input type="hidden" name="idPaciente" value="<?php echo $row->idPaciente; ?>"></input>
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </td>
                                    </tr>
                                        
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>      
            </section>
        </div>       
                         
                                      
                                            
                                        