        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">      
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                 
                    <li>
                       <li>
                        <a href="<?=base_url()?>index.php/Welcome/index2">
                            <i class="fa fa-home"></i> <span>Inicio</span>
                        </a>
                    </li>
                    </li>
                    <li class="treeview">
                         <a href="#">
                            <i class="fa fa-user"></i> <span>Usuarios</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url();?>usuarios/persona/listaPersona"><i class="fa fa-circle-o"></i> Persona</a></li>
                            <li><a href="<?php echo base_url();?>usuarios/usuario/listaUsuario"><i class="fa fa-circle-o"></i> Usuario</a></li>
                           
                        
                        </ul>
                    </li>
                    <li class="treeview">
                          <a href="#">
                            <i class="fa fa-male"></i> <span>Paciente</span> <!-- Contenido -->
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                         <ul class="treeview-menu">
                             <li><a href="<?php echo base_url();?>paciente/pacientee/listaPaciente"><i class="fa fa-circle-o"></i> Paciente</a></li>
                            <li><a href="<?php echo base_url();?>paciente/historiaclinica/listaHistoriaClinica"><i class="fa fa-circle-o"></i> Historia Clinica</a></li>
                            <li><a href="<?php echo base_url();?>paciente/tratamiento/listaTratamiento"><i class="fa fa-circle-o"></i> Tratamiento</a></li>
                            
                            <li><a href="<?php echo base_url();?>paciente/agenda/listaAgenda"><i class="fa fa-circle-o"></i> Agenda Citas</a></li>
                            <li><a href="<?php echo base_url();?>paciente/cuentacobrar/listaCuentaCobrar"><i class="fa fa-circle-o"></i> Cuenta por Cobrar</a></li>
                            <li><a href="<?php echo base_url();?>paciente/pacientee/listaPaciente"><i class="fa fa-circle-o"></i> Pagos</a></li>
                            
                        </ul>
                    </li>
                    <li class="treeview">
                       <a href="#">
                            <i class="fa fa-share-alt"></i> <span>Transaccion</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                        <li><a href="<?php echo base_url();?>transaccion/historial"><i class="fa fa-circle-o"></i>Registro Nuevo</a></li>
                        <li><a href="<?php echo base_url();?>odontoo/piezas/listaPieza"><i class="fa fa-circle-o"></i> Piezas Dentales</a></li>
                        <li><a href="<?php echo base_url();?>odontoo/odontograma/listaOdontograma"><i class="fa fa-circle-o"></i>-------</a></li>
                            
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-print"></i> <span>Reportes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> pacientes</a></li>
                            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> pagos</a></li>
                            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> agenda</a></li>
                            
                           
                        </ul>
                    </li>
                    
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->