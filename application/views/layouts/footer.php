        <footer class="main-footer">
           <!--  <div class="pull-right hidden-xs">
                <b>VERSION 2.0</b>
            </div> -->
          <!--  <strong>Copyright &copy;  <a href="https://infolibweb.com">@SISVENMUEBLES</a></strong> -->
        </footer>
    </div>
    <!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/template/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/template/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/template/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/template/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/template/dist/js/demo.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/template/css">




<script>

$(document).ready(function () {
    var base_url="<?php echo base_url();?>";

// para busqueda de las tablas

    $('#tabel').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    $('.sidebar-menu').tree()




$('.sidebar-menu').tree();
// para mostrar a los clientes para venta
$(document).on("click",".btn-check",function(){
    pacientee= $(this).val();
    infopaciente=pacientee.split("*");
    $("#idPaciente").val(infopaciente[0]);
    $("#paciente").val(infopaciente[1]+' '+infopaciente[2]+' '+infopaciente[3]);
    $("#modal-defaul").modal("hide");
    
});

$("#tratamiento").autocomplete({
    source:function(request, response){
        $.ajax({
            url: base_url+"transaccion/historial/getHistorial",
            type: "POST",
            dataType:"json",
            data:{ valor: request.term},
            success:function(data){
                response(data);
            }
        });
    },
    minLength:2,
    select:function(event, ui){
        data = ui.item.idTratamiento + "*"+ ui.item.label+ "*"+ ui.item.descripcion+ "*"+ ui.item.precio
        $("#btn-agregar").val(data);
    },
});

$("#btn-agregar").on("click",function(){
    data = $(this).val();
    if (data !='') {
        infotratamiento = data.split("*");   
        html = "<tr>";
        html += "<td><input type='hidden' name='idTratamiento[]' value='"+infotratamiento[0]+"'>"+infotratamiento[1]+"</td>";
         html += "<td><input type='text' name='nroPieza[]' value='11,22,12' class='nroPieza'></td>";
        html += "<td><input type='hidden' name='precio[]' value='"+infotratamiento[3]+"'>"+infotratamiento[3]+"</td>";
        html += "<td><input type='text' name='cantidades[]' value='1' class='cantidades'></td>";
        html += "<td><input type='hidden' name='importes[]' value='"+infotratamiento[3]+"'><p>"+infotratamiento[3]+"</p></td>";
        html += "<td><button type='button' class='btn btn-success btn-remove-tratamiento'><span class='fa fa-remove'></span></button></td>";
        html +="</tr>";
        $("#tbTratamiento tbody").append(html);
        sumar();
        $("#btn-agregar").val(null);
         $("#tratamiento").val(null);

    }else{
        alert("seleccione un tratamiento...");
    }
});
$(document).on("click",".btn-remove-tratamiento", function(){
    $(this).closest("tr").remove();
    sumar();
 });

 $(document).on("keyup","#tbTratamiento input.cantidades", function(){
    cantidad = $(this).val();
    precio = $(this).closest("tr").find("td:eq(2)").text();
    importe = cantidad * precio;
    $(this).closest("tr").find("td:eq(4)").children("p").text(importe);
     $(this).closest("tr").find("td:eq(4)").children("input").val(importe.toFixed(2));
    sumar();
 });

})



</script>













<script>
$(document).ready(function () {
    var base_url="<?php echo base_url();?>";

// para busqueda de las tablas
    $('#example1').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('.sidebar-menu').tree();
    })





$(document).on("click",".btn-checkk",function(){
    producto = $(this).val();

    
    infoproducto  = producto.split("*");  
   

        html = "<tr>";
        html += "<td><input type='hidden' name='idProducto[]' value='"+infoproducto[0]+"'>"+infoproducto[1]+"</td>";
        html += "<td>"+infoproducto[2]+"</td>";
        html += "<td><input type='hidden' name='precio[]' value='"+infoproducto[3]+"'>"+infoproducto[3]+"</td>";
        html += "<td>"+infoproducto[4]+"</td>";
        html += "<td><input type='text' name='cantidad[]' value='1' class='cantidad'></td>";
        html += "<td><input type='hidden' name='importe[]' value='"+infoproducto[3]+"'><p>"+infoproducto[3]+"</p></td>";
        html += "<td><button type='button' class='btn btn-danger btn-remove-producto'><span class='fa fa-remove'></span></button></td>";
        html +="</tr>";
    
    //if  (infoproducto != infoproducto[0]) {
        $("#tbventas tbody").append(html);
        sumar();
        $("#btn-checkk").val(null);
        $("#modal-defaull").modal("hide");
        //$("#producto").val(null);

    //}else{
    //alert("Ya inserto ese producto...");
    //}
});
// para eliminar product
$(document).on("click",".btn-remove-producto",function(){
    $(this).closest("tr").remove();
    sumar();
});
//para la cantidad
$(document).on("keyup","#tbventas input.cantidad", function(){
    cantidad = $(this).val();
    precio = $(this).closest("tr").find("td:eq(2)").text();
    importe = cantidad * precio;
    $(this).closest("tr").find("td:eq(5)").children("p").text(importe.toFixed(2));
     $(this).closest("tr").find("td:eq(5)").children("input").val(importe.toFixed(2));
    sumar();
 });
 function sumar(){
    subtotal=0;
    $("#tbventas tbody tr").each(function(){
        subtotal = subtotal + Number($(this).find("td:eq(5)").text());

    });
    $("input[name=subtotal]").val(subtotal.toFixed(2));
    descuento = $("input[name=descuento]").val();
    total = subtotal - descuento;
    $("input[name=total]").val(total.toFixed(2));
}
</script>




</body>
</html>
